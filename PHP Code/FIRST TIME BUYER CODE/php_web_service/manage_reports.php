<?php 

$page_title="Songs Reports";

include("includes/header.php");
require("includes/function.php");
require("language/language.php");


$sql_query="SELECT tbl_reports.*, tbl_users.`name`, tbl_users.`email`, tbl_mp3.`mp3_title` 
FROM tbl_reports
LEFT JOIN tbl_mp3 ON tbl_reports.`song_id`=tbl_mp3.`id`
LEFT JOIN tbl_users ON tbl_reports.`user_id`=tbl_users.`id`
ORDER BY tbl_reports.`id` DESC";

$result=mysqli_query($mysqli,$sql_query);

?>
<div class="row">
  <div class="col-xs-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card mrg_bottom">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 mrg-top">
        <table class="datatable table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Song</th>
              <th>Report</th> 
              <th>Datetime</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
           <?php
           $i=1;
           while($row=mysqli_fetch_array($result))
           {
            ?>
            <tr>
              <td width="50"><?=$i++?></td>
              <td style="word-wrap: break-all;"><?php echo $row['name'];?></td>
              <td style="word-wrap: break-all;"><?php echo $row['email'];?></td>
              <td style="word-wrap: break-all;"><?php echo $row['mp3_title'];?></td>
              <td style="word-wrap: break-all;"><?php echo $row['report'];?></td>
              <td>
                <?=(!is_null($row['created_at'])) ? date('d-m-Y', $row['created_at']).'<br/>'.date('h:i A', $row['created_at']) : '-'?>
              </td>                  
             <td>
                <a href="" class="btn btn-danger btn_delete btn_delete_a" data-table="tbl_reports" data-id="<?php echo $row['id'];?>"  data-toggle="tooltip" data-tooltip="Delete"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
</div>            

<?php include("includes/footer.php");?>  
