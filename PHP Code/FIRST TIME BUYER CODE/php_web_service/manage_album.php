<?php
$page_title="Manage Album"; 

include("includes/header.php");

require("includes/function.php");
require("language/language.php");

$tableName="tbl_album";   
$targetpage = "manage_album.php"; 
$limit = 12; 

$keyword='';

if(!isset($_GET['keyword'])){
  $query = "SELECT COUNT(*) as num FROM $tableName";
}
else{

  $keyword=addslashes(trim($_GET['keyword']));

  $query = "SELECT COUNT(*) as num FROM $tableName WHERE `album_name` LIKE '%$keyword%'";

  $targetpage = "manage_album.php?keyword=".$_GET['keyword'];

}

$total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
$total_pages = $total_pages['num'];

$stages = 3;
$page=0;
if(isset($_GET['page'])){
  $page = mysqli_real_escape_string($mysqli,$_GET['page']);
}
if($page){
  $start = ($page - 1) * $limit; 
}else{
  $start = 0; 
} 

if(!isset($_GET['keyword'])){
  $sql_query="SELECT * FROM tbl_album ORDER BY tbl_album.`aid` DESC LIMIT $start, $limit"; 
}
else{

  $sql_query="SELECT * FROM tbl_album WHERE `album_name` LIKE '%$keyword%' ORDER BY tbl_album.`aid` DESC LIMIT $start, $limit"; 
}

$result=mysqli_query($mysqli,$sql_query) or die(mysqli_error($mysqli));


function get_total_songs($album_id)
{ 
  global $mysqli;   

  $qry_songs="SELECT COUNT(*) as num FROM tbl_mp3 WHERE album_id='".$album_id."'";

  $total_songs = mysqli_fetch_array(mysqli_query($mysqli,$qry_songs));
  $total_songs = $total_songs['num'];

  return $total_songs;
}

?>

<div class="row">
  <div class="col-xs-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card mrg_bottom">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
        <div class="col-md-7 col-xs-12">
          <div class="search_list">
            <div class="search_block">
              <form method="get" action="">
                <input class="form-control input-sm" placeholder="Search here..." aria-controls="DataTables_Table_0" type="search" name="keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" required="required">
                <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
              </form>  
            </div>
            <div class="add_btn_primary"> <a href="add_album.php?add=yes">Add Album</a> </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 mrg-top">
        <div class="row">
          <?php 
          $i=0;
          while($row=mysqli_fetch_array($result))
          {         
            ?>
            <div class="col-lg-3 col-sm-6 col-xs-12">
              <div class="block_wallpaper">           
                <div class="wall_image_title">
                  <h2>
                    <a href="add_album.php?album_id=<?php echo $row['aid'];?>&redirect=<?=$redirectUrl?>" title="<?=$row['album_name']?>">
                      <?php
                      if (strlen($row['album_name']) > 18) {
                        echo substr(stripslashes($row['album_name']), 0, 18) . '...';
                      } else {
                        echo $row['album_name'];
                      }
                      ?>
                      <span>(<?php echo get_total_songs($row['aid']);?>)</span>
                    </a>
                  </h2>
                  <ul>                
                    <li><a href="add_album.php?album_id=<?php echo $row['aid'];?>&redirect=<?=$redirectUrl?>" data-toggle="tooltip" data-tooltip="Edit"><i class="fa fa-edit"></i></a></li>

                    <li>
                      <a href="javascript:void(0)" class="btn_delete_a" data-table="tbl_album" data-id="<?php echo $row['aid'];?>"  data-toggle="tooltip" data-tooltip="Delete"><i class="fa fa-trash"></i></a>
                    </li>

                    <li>
                    <div class="row toggle_btn">
                      <input type="checkbox" id="enable_disable_check_<?=$i?>" data-id="<?=$row['aid']?>" data-table="tbl_album" data-column="status" class="cbx hidden enable_disable" <?php if($row['status']==1){ echo 'checked';} ?>>
                      <label for="enable_disable_check_<?=$i?>" class="lbl"></label>
                    </div>
                  </li>

                  </ul>
                </div>
                <span><img style="height:230px;" src="images/<?php echo $row['album_image'];?>" /></span>
              </div>
            </div>
            <?php

            $i++;
          }
          ?>     

        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="pagination_item_block">
          <nav>
            <?php include("pagination.php")?>
          </nav>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>


<?php include("includes/footer.php");?>       

