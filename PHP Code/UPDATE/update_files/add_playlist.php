<?php

$page_title=(isset($_GET['playlist_id'])) ? 'Edit Playlist' : 'Add Playlist';

include("includes/header.php");

require("includes/function.php");
require("language/language.php");

require_once("thumbnail_images.class.php");

if(isset($_POST['submit']) and isset($_GET['add']))
{

  $ext = pathinfo($_FILES['playlist_image']['name'], PATHINFO_EXTENSION);

  $playlist_image=rand(0,99999)."_playlist.".$ext;

  $tpath1='images/'.$playlist_image;   

  if($ext!='png')  {
    $pic1=compress_image($_FILES["playlist_image"]["tmp_name"], $tpath1, 80);
  }
  else{
    $tmp = $_FILES['playlist_image']['tmp_name'];
    move_uploaded_file($tmp, $tpath1);
  }

  $thumbpath='images/thumbs/'.$playlist_image;   
  $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'300','300');   


  $data = array( 
    'playlist_name'  =>  cleanInput($_POST['playlist_name']),
    'playlist_image'  =>  $playlist_image,
    'playlist_songs'  =>  implode(',',$_POST['playlist_songs'])
  );    

  $qry = Insert('tbl_playlist',$data);  

  $_SESSION['msg']="10";
  $_SESSION['class']='success';

  header( "Location:manage_playlist.php");
  exit;
}

if(isset($_GET['playlist_id']))
{

  $qry="SELECT * FROM tbl_playlist WHERE `pid`='".$_GET['playlist_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);

}

if(isset($_GET['playlist_id'])){
  $mp3_qry="SELECT * FROM tbl_mp3 WHERE tbl_mp3.`id` IN (".$row['playlist_songs'].") ORDER BY tbl_mp3.id DESC"; 
}
else{
  $mp3_qry="SELECT * FROM tbl_mp3 ORDER BY tbl_mp3.`id` DESC LIMIT 0, 10"; 
}

$mp3_result=mysqli_query($mysqli,$mp3_qry); 

if(isset($_POST['submit']) and isset($_POST['playlist_id']))
{
  if($_FILES['playlist_image']['name']!="")
  {
    if($row['playlist_image']!="")
    {
      unlink('images/thumbs/'.$row['playlist_image']);
      unlink('images/'.$row['playlist_image']);
    }

    $ext = pathinfo($_FILES['playlist_image']['name'], PATHINFO_EXTENSION);

    $playlist_image=rand(0,99999)."_playlist.".$ext;

    $tpath1='images/'.$playlist_image;   

    if($ext!='png')  {
      $pic1=compress_image($_FILES["playlist_image"]["tmp_name"], $tpath1, 80);
    }
    else{
      $tmp = $_FILES['playlist_image']['tmp_name'];
      move_uploaded_file($tmp, $tpath1);
    }

    $thumbpath='images/thumbs/'.$playlist_image;   
    $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'300','300');

    $data = array(
      'playlist_name'  =>  cleanInput($_POST['playlist_name']),
      'playlist_image'  =>  $playlist_image,
      'playlist_songs'  =>  implode(',',$_POST['playlist_songs'])
    );

    $update=Update('tbl_playlist', $data, "WHERE pid = '".$_POST['playlist_id']."'");
  }
  else
  {

    $data = array(
      'playlist_name'  =>  cleanInput($_POST['playlist_name']),
      'playlist_songs'  =>  implode(',',$_POST['playlist_songs'])
    );  

    $update=Update('tbl_playlist', $data, "WHERE pid = '".$_POST['playlist_id']."'");
  }

  $_SESSION['msg']="11";
  $_SESSION['class']='success'; 

  if(isset($_GET['redirect'])){
    header("Location:".$_GET['redirect']);
  }
  else{
    header( "Location:add_playlist.php?playlist_id=".$_POST['playlist_id']);
  }
  exit;

}


?>
<div class="row">
  <div class="col-md-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="card-body mrg_bottom"> 
        <form action="" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
          <input  type="hidden" name="playlist_id" value="<?php echo $_GET['playlist_id'];?>" />

          <div class="section">
            <div class="section-body">

              <div class="form-group">
                <label class="col-md-3 control-label">Playlist Name :-</label>
                <div class="col-md-6">
                  <input type="text" name="playlist_name" id="playlist_name" value="<?php if(isset($_GET['playlist_id'])){echo $row['playlist_name'];}?>" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Select Image :-
                  <p class="control-label-help">(Recommended resolution: 300x300, 400x400 OR Square Image)</p>
                </label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="playlist_image" value="fileupload" onchange="fileValidation()" id="fileupload">
                    <?php if(isset($_GET['playlist_id'])) {?>
                      <div class="fileupload_img" id="uploadPreview"><img type="image" src="images/<?php echo $row['playlist_image'];?>" alt="image" style="width: 100px;height: 100px;"/></div>
                    <?php }else{?>
                      <div class="fileupload_img" id="uploadPreview"><img type="image" src="assets/images/square.jpg" alt="image" style="width: 100px;height: 100px" /></div>
                    <?php } ?>

                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Songs :-</label>
                <div class="col-md-6">
                  <?php if(isset($_GET['playlist_id'])){?>
                    <input type="hidden" value="<?=$row['playlist_songs']?>" id="search">
                  <?php }else{?>
                    <input type="hidden" value="" id="search">
                  <?php }?>  

                  <select name="playlist_songs[]" id="playlist_songs" class="select2 form-control" required multiple="multiple">
                    <option value="">--Select Songs--</option>
                    <?php
                    while($mp3_row=mysqli_fetch_array($mp3_result))
                    {
                      ?>   
                      <?php if(isset($_GET['playlist_id'])){?>

                        <option value="<?php echo $mp3_row['id'];?>" <?php $songs_list=explode(",", $row['playlist_songs']);foreach($songs_list as $song_id){ if($mp3_row['id']==$song_id){ echo 'selected="selected"'; }}?>><?php echo $mp3_row['mp3_title'];?></option>

                      <?php }else{?>  

                        <option value="<?php echo $mp3_row['id'];?>"><?php echo $mp3_row['mp3_title'];?></option>

                      <?php }?>   

                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php include("includes/footer.php");?>

<script type="text/javascript">

  $(function(){
    $('.select2').select2({
      ajax: {
        url: 'getData.php',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          var query = {
            type: 'song',
            search: params.term,
            page: params.page || 1
          }
          return query;
        },
        processResults: function (data, params) {
          params.page = params.page || 1;
          return {
            results: data.items,
            pagination: {
              more: (params.page * 5) < data.total_count
            }
          };
        },
        cache: true
      }
    });
  });


  function fileValidation(){
    var fileInput = document.getElementById('fileupload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/i;
    if(!allowedExtensions.exec(filePath)){
      if(filePath!=''){
        infoDlg = duDialog('Opps!', 'Please upload file having extension .png, .jpg, .jpeg .PNG, .JPG, .JPEG only!', { init: null,dark: false})
      }
      fileInput.value = '';
      return false;
    }else{
      if (fileInput.files && fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          document.getElementById('uploadPreview').innerHTML = '<img src="'+e.target.result+'" style="width:100px;height:100px"/>';
        };
        reader.readAsDataURL(fileInput.files[0]);
      }
    }
  }

</script>