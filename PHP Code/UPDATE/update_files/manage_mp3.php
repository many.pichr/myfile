<?php

$page_title = "Manage Mp3";

include("includes/header.php");
require("includes/function.php");
require("language/language.php");


$tableName = "tbl_mp3";
$targetpage = "manage_mp3.php";
$limit = 12;

$keyword = '';

if (isset($_GET['category'])) {

  $category = trim($_GET['category']);
  $album = '';

  $targetpage = "manage_mp3.php?category=" . $category;

  $query = "SELECT COUNT(*) as num FROM $tableName WHERE `cat_id`=" . $category;

  if (isset($_GET['album'])) {

    $album = $_GET['album'];

    $query = "SELECT COUNT(*) as num FROM $tableName WHERE `cat_id`= $category  AND `album_id` = $album";

    $targetpage = "manage_mp3.php?category=" . $category . "&album=" . $album;
  }

  $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
  $total_pages = $total_pages['num'];

  $stages = 3;
  $page = 0;
  if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($mysqli, $_GET['page']);
  }
  if ($page) {
    $start = ($page - 1) * $limit;
  } else {
    $start = 0;
  }

  if (isset($_GET['album'])) {
    $sql_query = "SELECT tbl_mp3.*, tbl_category.`cid`, tbl_category.`category_name` FROM tbl_mp3 LEFT JOIN tbl_category ON tbl_mp3.`cat_id` = tbl_category.`cid` WHERE tbl_mp3.`cat_id` = '$category' AND tbl_mp3.`album_id` = '$album' ORDER BY tbl_mp3.`id` DESC LIMIT $start, $limit";
  } else {
    $sql_query = "SELECT tbl_mp3.*, tbl_category.`cid`, tbl_category.`category_name` FROM tbl_mp3 LEFT JOIN tbl_category ON tbl_mp3.`cat_id` = tbl_category.`cid` WHERE tbl_mp3.`cat_id` = '$category' ORDER BY tbl_mp3.`id` DESC LIMIT $start, $limit";
  }
} else if (isset($_GET['album'])) {

  $album = trim($_GET['album']);

  $query = "SELECT COUNT(*) as num FROM $tableName WHERE `album_id` = $album";

  $targetpage = "manage_mp3.php?album=" . $album;

  $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
  $total_pages = $total_pages['num'];

  $stages = 3;
  $page = 0;
  if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($mysqli, $_GET['page']);
  }
  if ($page) {
    $start = ($page - 1) * $limit;
  } else {
    $start = 0;
  }

  $sql_query = "SELECT tbl_mp3.*, tbl_category.`cid`, tbl_category.`category_name` FROM tbl_mp3 LEFT JOIN tbl_category ON tbl_mp3.`cat_id` = tbl_category.`cid` WHERE tbl_mp3.`album_id` = '$album' ORDER BY tbl_mp3.`id` DESC LIMIT $start, $limit";
} 
else if (isset($_GET['keyword'])) {

  $keyword = addslashes(trim($_GET['keyword']));

  $query = "SELECT COUNT(*) as num FROM $tableName WHERE (`mp3_title` LIKE '%$keyword%' OR `mp3_description` LIKE '%$keyword%')";

  $targetpage = "manage_mp3.php?keyword=" . $_GET['keyword'];

  $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
  $total_pages = $total_pages['num'];

  $stages = 3;
  $page = 0;
  if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($mysqli, $_GET['page']);
  }
  if ($page) {
    $start = ($page - 1) * $limit;
  } else {
    $start = 0;
  }

  $sql_query = "SELECT tbl_mp3.*, tbl_category.`cid`, tbl_category.`category_name` FROM tbl_mp3 LEFT JOIN tbl_category ON tbl_mp3.`cat_id` = tbl_category.`cid` WHERE (`mp3_title` LIKE '%$keyword%' OR `mp3_description` LIKE '%$keyword%') ORDER BY tbl_mp3.`id` DESC LIMIT $start, $limit";
} 
else {
  $query = "SELECT COUNT(*) as num FROM $tableName";
  $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
  $total_pages = $total_pages['num'];

  $stages = 3;
  $page = 0;
  if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($mysqli, $_GET['page']);
  }
  if ($page) {
    $start = ($page - 1) * $limit;
  } else {
    $start = 0;
  }

  $sql_query = "SELECT tbl_mp3.`id`,tbl_mp3.`mp3_title`,tbl_mp3.`total_views`,tbl_mp3.`total_download`,tbl_mp3.`rate_avg`,tbl_mp3.`mp3_thumbnail`,tbl_mp3.`status`, tbl_category.`cid`, tbl_category.`category_name` FROM tbl_mp3 LEFT JOIN tbl_category ON tbl_mp3.`cat_id` = tbl_category.`cid` ORDER BY tbl_mp3.`id` DESC LIMIT $start, $limit";
}

$result = mysqli_query($mysqli, $sql_query) or die(mysqli_error($mysqli));

function get_total_songs($cat_id)
{

  global $mysqli;
  $qry_songs = "SELECT COUNT(*) as num FROM tbl_mp3 WHERE cat_id='" . $cat_id . "'";
  $total_songs = mysqli_fetch_array(mysqli_query($mysqli, $qry_songs));
  $total_songs = $total_songs['num'];
  return $total_songs;
}
?>

<div class="row">
  <div class="col-xs-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card mrg_bottom">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?= $page_title ?></div>
        </div>
        <div class="col-md-7 col-xs-12">
          <div class="search_list">
            <div class="search_block">
              <form method="get" action="">
                <input class="form-control input-sm" placeholder="Search here..." aria-controls="DataTables_Table_0" type="search" name="keyword" value="<?php if (isset($_GET['keyword'])) { echo $_GET['keyword']; } ?>" required="required">
                <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
              </form>
            </div>
            <div class="add_btn_primary"> <a href="add_mp3.php">Add Mp3</a> </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <form id="filterForm" accept="" method="GET">
          <div class="col-md-3 col-xs-12">
            <select name="category" class="form-control select2 filter" data-type="category" style="padding: 5px 10px;height: 40px;">
              <option value="">All Category</option>
              <?php
              if (!isset($_GET['category'])){
                $sql_cat = "SELECT * FROM tbl_category ORDER BY category_name LIMIT 10";
              }
              else{
               $sql_cat = "SELECT * FROM tbl_category WHERE `cid`='".$_GET['category']."' ORDER BY category_name LIMIT 10"; 
             }
             $res_cat = mysqli_query($mysqli, $sql_cat);
             while ($row_cat = mysqli_fetch_array($res_cat)) {
              ?>
              <option value="<?php echo $row_cat['cid']; ?>" <?php if (isset($_GET['category']) && $_GET['category'] == $row_cat['cid']) { echo 'selected'; } ?>><?php echo $row_cat['category_name'], ' (' . get_total_songs($row_cat['cid']) . ')'; ?></option>
              <?php
            }
           
            ?>
          </select>
        </div>
        <div class="col-md-3 col-xs-12">
          <select name="album" class="form-control select2 filter" data-type="album" style="padding: 5px 10px;height: 40px;">
            <option value="">All Album</option>
            <?php
            if (!isset($_GET['album'])){
              $sql_album = "SELECT * FROM tbl_album ORDER BY album_name LIMIT 10";
            }
            else{
              $sql_album = "SELECT * FROM tbl_album WHERE `aid`='".$_GET['album']."' ORDER BY album_name LIMIT 10";
            }
            
            $res_album = mysqli_query($mysqli, $sql_album);
            while ($row_album = mysqli_fetch_array($res_album)) {
              ?>
              <option value="<?php echo $row_album['aid']; ?>" <?php if (isset($_GET['album']) && $_GET['album'] == $row_album['aid']) { echo 'selected'; } ?>><?php echo $row_album['album_name']; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </form>

      <div class="col-md-4 col-xs-12 text-right" style="float: right;">
        <div>
          <div class="checkbox" style="width: 95px;margin-top: 5px;margin-left: 10px;right: 100px;position: absolute;">
            <input type="checkbox" id="checkall_input">
            <label for="checkall_input">
              Select All
            </label>
          </div>
          <div class="dropdown" style="float:right">
            <button class="btn btn-primary dropdown-toggle btn_cust" type="button" data-toggle="dropdown">Action
              <span class="caret"></span></button>
              <ul class="dropdown-menu" style="right:0;left:auto;">
              <li><a href="javascript:void(0)" class="actions" data-action="enable" data-table="tbl_mp3" data-column="status">Enable</a></li>
              <li><a href="javascript:void(0)" class="actions" data-action="disable" data-table="tbl_mp3" data-column="status">Disable</a></li>
              <li><a href="javascript:void(0)" class="actions" data-action="delete" data-table="tbl_mp3" data-column="status">Delete !</a></li>
            </ul>
            </div>
          </div>
        </div>

      </div>
      <div class="clearfix"></div>

      <div class="col-md-12 mrg-top">
        <div class="row">
          <?php
          $i = 0;
          while ($row = mysqli_fetch_array($result)) {
            $mp3_file = $row['mp3_url'];

            if ($row['mp3_type'] == 'local') {
              $mp3_file = $file_path . 'uploads/' . basename($row['mp3_url']);
            }

            ?>
            <div class="col-lg-4 col-sm-6 col-xs-12">
              <div class="block_wallpaper">
                <div class="wall_category_block">
                  <h2>
                    <?php
                    if (strlen($row['category_name']) > 18) {
                      echo substr(stripslashes($row['category_name']), 0, 18) . '...';
                    } else {
                      echo $row['category_name'];
                    }
                    ?>
                  </h2>

                  <div class="checkbox" style="float: right;">
                    <input type="checkbox" name="post_ids[]" id="checkbox<?php echo $i; ?>" value="<?php echo $row['id']; ?>" class="post_ids">
                    <label for="checkbox<?php echo $i; ?>">
                    </label>
                  </div>
                </div>
                <div class="wall_image_title">
                  <p>
                    <?php
                    if (strlen($row['mp3_title']) > 30) {
                      echo substr(stripslashes($row['mp3_title']), 0, 29) . '...';
                    } else {
                      echo $row['mp3_title'];
                    }
                    ?>
                  </p>
                  <ul>
                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-tooltip="<?php echo thousandsNumberFormat($row['total_views']); ?> Views"><i class="fa fa-eye"></i></a></li>

                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-tooltip="<?php echo thousandsNumberFormat($row['total_download']); ?> Download"><i class="fa fa-download"></i></a></li>

                    <li><a href="javascript:void(0)" data-toggle="tooltip" data-tooltip="<?php echo $row['rate_avg']; ?> Rating"><i class="fa fa-star"></i></a></li>

                    <li><a href="edit_mp3.php?mp3_id=<?php echo $row['id']; ?>&redirect=<?= $redirectUrl ?>" data-toggle="tooltip" data-tooltip="Edit"><i class="fa fa-edit"></i></a></li>

                    <li>
                    <a href="javascript:void(0)" class="btn_delete_a" data-table="tbl_mp3" data-id="<?php echo $row['id'];?>"  data-toggle="tooltip" data-tooltip="Delete"><i class="fa fa-trash"></i></a>
                  </li>
                  <li>
                    <div class="row toggle_btn">
                      <input type="checkbox" id="enable_disable_check_<?=$i?>" data-id="<?=$row['id']?>" data-table="tbl_mp3" data-column="status" class="cbx hidden enable_disable" <?php if($row['status']==1){ echo 'checked';} ?>>
                      <label for="enable_disable_check_<?=$i?>" class="lbl"></label>
                    </div>
                  </li>

                  </ul>
                </div>
                <span><img src="images/<?php echo $row['mp3_thumbnail']; ?>" /></span>
              </div>
            </div>
            <?php

            $i++;
          }
          ?>

        </div>
      </div>
      <div class="col-md-12 col-xs-12">
        <div class="pagination_item_block">
          <nav>
            <?php include("pagination.php") ?>
          </nav>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<?php include("includes/footer.php"); ?>

<script type="text/javascript">
  
 $(function(){
    $('.select2').select2({
      ajax: {
        url: 'getData.php',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          var query = {
            type: $(this).data("type"),
            search: params.term,
            page: params.page || 1
          }
          return query;
        },
        processResults: function (data, params) {
         params.page = params.page || 1;
         return {
          results: data.items,
          pagination: {
            more: (params.page * 5) < data.total_count
          }
        };
      },
      cache: true
    }
  });
});

  
</script>