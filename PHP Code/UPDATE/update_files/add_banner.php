<?php

$page_title=(isset($_GET['banner_id'])) ? 'Edit Banner' : 'Add Banner';

$active_page = "android_app";

include("includes/header.php");

require("includes/function.php");
require("language/language.php");

require_once("thumbnail_images.class.php");

if(isset($_POST['submit']) and isset($_GET['add']))
{

  $ext = pathinfo($_FILES['banner_image']['name'], PATHINFO_EXTENSION);

  $banner_image=rand(0,99999)."_banner.".$ext;

  $tpath1='images/'.$banner_image;   

  if($ext!='png')  {
    $pic1=compress_image($_FILES["banner_image"]["tmp_name"], $tpath1, 80);
  }
  else{
    $tmp = $_FILES['banner_image']['tmp_name'];
    move_uploaded_file($tmp, $tpath1);
  }

  $thumbpath='images/thumbs/'.$banner_image;   
  $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'300','200');   

  $data = array( 
    'banner_title'  =>  cleanInput($_POST['banner_title']),
    'banner_sort_info'  =>  addslashes(trim($_POST['banner_sort_info'])),
    'banner_image'  =>  $banner_image,
    'banner_songs'  =>  implode(',',$_POST['banner_songs'])
  );    

  $qry = Insert('tbl_banner',$data);  

  $_SESSION['msg']="10";
  $_SESSION['class']='success';

  header( "Location:manage_banners.php");
  exit;
}

if(isset($_GET['banner_id']))
{

  $qry="SELECT * FROM tbl_banner WHERE `bid`='".$_GET['banner_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);

}

if(isset($_GET['banner_id'])){
  $mp3_qry="SELECT * FROM tbl_mp3 WHERE tbl_mp3.`id` IN (".$row['banner_songs'].") ORDER BY tbl_mp3.id DESC"; 
}
else{
  $mp3_qry="SELECT * FROM tbl_mp3 ORDER BY tbl_mp3.`id` DESC LIMIT 0, 10"; 
}

$mp3_result=mysqli_query($mysqli,$mp3_qry); 

if(isset($_POST['submit']) and isset($_POST['banner_id']))
{
 if($_FILES['banner_image']['name']!="")
 {
  if($row['banner_image']!="")
  {
    unlink('images/thumbs/'.$row['banner_image']);
    unlink('images/'.$row['banner_image']);
  }

  $ext = pathinfo($_FILES['banner_image']['name'], PATHINFO_EXTENSION);

  $banner_image=rand(0,99999)."_banner.".$ext;

  $tpath1='images/'.$banner_image;   

  if($ext!='png')  {
    $pic1=compress_image($_FILES["banner_image"]["tmp_name"], $tpath1, 80);
  }
  else{
    $tmp = $_FILES['banner_image']['tmp_name'];
    move_uploaded_file($tmp, $tpath1);
  }

  $thumbpath='images/thumbs/'.$banner_image;   
  $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'300','300');

  $data = array(
    'banner_title'  =>  cleanInput($_POST['banner_title']),
    'banner_sort_info'  =>  addslashes(trim($_POST['banner_sort_info'])),
    'banner_image'  =>  $banner_image,
    'banner_songs'  =>  implode(',',$_POST['banner_songs'])
  );

  $update=Update('tbl_banner', $data, "WHERE bid = '".$_POST['banner_id']."'");
}
else
{

  $data = array(
    'banner_title'  =>  cleanInput($_POST['banner_title']),
    'banner_sort_info'  =>  addslashes(trim($_POST['banner_sort_info'])),
    'banner_songs'  =>  implode(',',$_POST['banner_songs'])
  );  

  $update=Update('tbl_banner', $data, "WHERE bid = '".$_POST['banner_id']."'");
}

$_SESSION['msg']="11";
$_SESSION['class']='success'; 

if(isset($_GET['redirect'])){
  header("Location:".$_GET['redirect']);
}
else{
  header( "Location:add_banner.php?banner_id=".$_POST['banner_id']);
}
exit;

}


?>
<div class="row">
  <div class="col-md-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="card-body mrg_bottom"> 
        <form action="" name="" method="post" class="form form-horizontal" enctype="multipart/form-data">
          <input  type="hidden" name="banner_id" value="<?php echo $_GET['banner_id'];?>" />

          <div class="section">
            <div class="section-body">

              <div class="form-group">
                <label class="col-md-3 control-label">Banner Title :-</label>
                <div class="col-md-6">
                  <input type="text" name="banner_title" id="banner_title" value="<?php if(isset($_GET['banner_id'])){echo $row['banner_title'];}?>" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Banner Sort Info :-</label>
                <div class="col-md-6">
                  <input type="text" name="banner_sort_info" id="banner_sort_info" value="<?php if(isset($_GET['banner_id'])){echo $row['banner_sort_info'];}?>" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Select Image :-
                  <p class="control-label-help">(Recommended resolution: 500x300, 600x360, 700X420 OR Horizontal Image)</p>
                </label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="banner_image" value="fileupload" onchange="fileValidation()" id="fileupload">
                    <?php if(isset($_GET['banner_id'])) {?>
                      <div class="fileupload_img" id="uploadPreview"><img type="image" src="images/<?php echo $row['banner_image'];?>" alt="image" style="width: 160px;height: 100px;"/></div>
                    <?php }else{?>
                      <div class="fileupload_img" id="uploadPreview"><img type="image" src="assets/images/square.jpg" alt="image" style="width: 160px;height: 100px" /></div>
                    <?php } ?>
                    
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Songs :-</label>
                <div class="col-md-6">
                  <?php if(isset($_GET['banner_id'])){?>
                    <input type="hidden" value="<?=$row['banner_songs']?>" id="search">
                  <?php }else{?>
                    <input type="hidden" value="" id="search">
                  <?php }?>  
                  
                  <select name="banner_songs[]" id="banner_songs" class="select2 form-control" required multiple="multiple">
                    <option value="">--Select Songs--</option>
                    <?php
                    while($mp3_row=mysqli_fetch_array($mp3_result))
                    {
                      ?>   
                      <?php if(isset($_GET['banner_id'])){?>

                       <option value="<?php echo $mp3_row['id'];?>" <?php $songs_list=explode(",", $row['banner_songs']);foreach($songs_list as $song_id){ if($mp3_row['id']==$song_id){ echo 'selected="selected"'; }}?>><?php echo $mp3_row['mp3_title'];?></option>

                     <?php }else{?>  

                      <option value="<?php echo $mp3_row['id'];?>"><?php echo $mp3_row['mp3_title'];?></option>
                      
                    <?php }?>   
                    
                    <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-9 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>

<?php include("includes/footer.php");?>       


<script type="text/javascript">

  $(function(){
    $('.select2').select2({
      ajax: {
        url: 'getData.php',
        dataType: 'json',
        delay: 250,
        data: function (params) {
          var query = {
            type: 'song',
            search: params.term,
            page: params.page || 1
          }
          return query;
        },
        processResults: function (data, params) {
         params.page = params.page || 1;
         return {
          results: data.items,
          pagination: {
            more: (params.page * 5) < data.total_count
          }
        };
      },
      cache: true
    }
  });
  });
  
  function fileValidation(){
    var fileInput = document.getElementById('fileupload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/i;
    if(!allowedExtensions.exec(filePath)){
      if(filePath!=''){
        infoDlg = duDialog('Opps!', 'Please upload file having extension .png, .jpg, .jpeg .PNG, .JPG, .JPEG only!', { init: null,dark: false})
      }
      fileInput.value = '';
      return false;
    }else{
      if (fileInput.files && fileInput.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          document.getElementById('uploadPreview').innerHTML = '<img src="'+e.target.result+'" style="width:160px;height:100px"/>';
        };
        reader.readAsDataURL(fileInput.files[0]);
      }
    }
  }

</script>