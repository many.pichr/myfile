<?php

$page_title="Edit Mp3"; 

include("includes/header.php");

require("includes/function.php");
require("language/language.php");

require_once("thumbnail_images.class.php");

$file_path=getBaseUrl();

$cat_qry="SELECT * FROM tbl_category ORDER BY `category_name`";
$cat_result=mysqli_query($mysqli,$cat_qry); 

$album_qry="SELECT * FROM tbl_album ORDER BY `album_name`";
$album_result=mysqli_query($mysqli,$album_qry); 

$art_qry="SELECT * FROM tbl_artist ORDER BY `artist_name`";
$art_result=mysqli_query($mysqli,$art_qry); 

$qry="SELECT * FROM tbl_mp3 WHERE `id`='".$_GET['mp3_id']."'";
$result=mysqli_query($mysqli,$qry);
$row=mysqli_fetch_assoc($result);

if(isset($_POST['submit']))
{

  $mp3_type=trim($_POST['mp3_type']);

  if($mp3_type=='server_url'){
    $mp3_url=htmlentities(trim($_POST['mp3_url']));

    if($row['mp3_type']=='local'){
      unlink('uploads/'.basename($row['mp3_url']));
    }

  }
  else{
    $path = "uploads/";

    if($_FILES['mp3_local']['name']!="")
    {

      unlink('uploads/'.basename($row['mp3_url']));

      $ext = pathinfo($_FILES['mp3_local']['name'], PATHINFO_EXTENSION);

      $mp3_local=rand(0,99999).$_POST['mp3_id']."_mp3.".$ext;

      $tmp = $_FILES['mp3_local']['tmp_name'];

      if (move_uploaded_file($tmp, $path.$mp3_local)) 
      {
        $mp3_url=$mp3_local;
      } else {
        echo "Error in uploading mp3 file !!";
        exit;
      }
    }
    else{
      $mp3_url=basename($row['mp3_url']);
    }
  }

  if($_FILES['mp3_thumbnail']['name']!="")
  {

    unlink('images/'.$row['mp3_thumbnail']);
    unlink('images/thumbs/'.$row['mp3_thumbnail']);

    $ext = pathinfo($_FILES['mp3_thumbnail']['name'], PATHINFO_EXTENSION);

    $mp3_thumbnail=rand(0,99999)."_mp3_thumb.".$ext;

    $tpath1='images/'.$mp3_thumbnail;   

    if($ext!='png')  {
      $pic1=compress_image($_FILES["mp3_thumbnail"]["tmp_name"], $tpath1, 80);
    }
    else{
      $tmp = $_FILES['mp3_thumbnail']['tmp_name'];
      move_uploaded_file($tmp, $tpath1);
    }

    $thumbpath='images/thumbs/'.$mp3_thumbnail;   
    $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200'); 
  }
  else{
    $mp3_thumbnail=$row['mp3_thumbnail'];
  }

  $data = array( 
    'cat_id'  =>  $_POST['cat_id'],
    'album_id'  =>  $_POST['album_id'],
    'mp3_title'  =>  $_POST['mp3_title'],
    'mp3_type'  =>  $_POST['mp3_type'],
    'mp3_url'  =>  $mp3_url,
    'mp3_thumbnail'  =>  $mp3_thumbnail,
    'mp3_duration'  =>  '-',
    'mp3_artist'  => implode(',', $_POST['mp3_artist']),
    'mp3_description'  =>  addslashes(trim($_POST['mp3_description']))
  );

  $qry=Update('tbl_mp3', $data, "WHERE id = '".$_POST['mp3_id']."'");


  $_SESSION['msg']="11"; 
  $_SESSION['class']="success"; 

  if(isset($_GET['redirect'])){
    header("Location:".$_GET['redirect']);
  }
  else{
    header( "Location:edit_mp3.php?mp3_id=".$_POST['mp3_id']);
  }
  exit;
}

?>
<div class="row">
  <div class="col-md-12">
   <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="card-body mrg_bottom"> 
        <form action=""  method="post" class="form form-horizontal" enctype="multipart/form-data">
          <input  type="hidden" name="mp3_id" value="<?php echo $_GET['mp3_id'];?>" />
          <div class="section">
            <div class="section-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Title :-</label>
                <div class="col-md-6">
                  <input type="text" name="mp3_title" id="mp3_title" value="<?php echo $row['mp3_title']?>" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Category :-</label>
                <div class="col-md-6">
                  <select name="cat_id" id="cat_id" class="select2">
                    <option value="">--Select Category--</option>
                    <?php
                    while($cat_row=mysqli_fetch_array($cat_result))
                    {
                      ?>                       
                      <option value="<?php echo $cat_row['cid'];?>" <?php if($cat_row['cid']==$row['cat_id']){?>selected<?php }?>><?php echo $cat_row['category_name'];?></option>                           
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Album :-</label>
                <div class="col-md-6">
                  <select name="album_id" id="album_id" class="select2">
                    <option value="">--Select Album--</option>
                    <?php
                    while($album_row=mysqli_fetch_array($album_result))
                    {
                      ?>                       
                      <option value="<?php echo $album_row['aid'];?>" <?php if($album_row['aid']==$row['album_id']){?>selected<?php }?>><?php echo $album_row['album_name'];?></option>                           
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Artist :-</label>
                <div class="col-md-6">
                  <select name="mp3_artist[]" id="mp3_artist" class="select2 form-control" required multiple="multiple">
                    <option value="">--Select Artist--</option>
                    <?php
                    while($art_row=mysqli_fetch_array($art_result))
                    {
                      ?>                       
                      <option value="<?php echo $art_row['artist_name'];?>" <?php if(in_array($art_row['artist_name'], explode(",",$row['mp3_artist']))){?>selected<?php }?>><?php echo $art_row['artist_name'];?></option>                           
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Upload From :-</label>
                <div class="col-md-6">                       
                  <select name="mp3_type" id="mp3_type" style="width:280px; height:25px;" class="select2" required>                           
                    <option value="server_url" <?php if($row['mp3_type']=='server_url'){?>selected<?php }?>>From Server(URL)</option>
                    <option value="local" <?php if($row['mp3_type']=='local'){?>selected<?php }?>>Browse From Device</option>
                  </select>
                </div>
              </div>
              <div id="mp3_url_display" class="form-group" <?php if($row['mp3_type']=='local'){?>style="display:none;"<?php }else{?>style="display:block;"<?php }?>>
                <label class="col-md-3 control-label">Song URL :-</label>
                <div class="col-md-6">
                  <input type="text" name="mp3_url" id="mp3_url" value="<?php echo $row['mp3_url']?>" class="form-control">
                </div>
              </div>
              <div id="mp3_local_display" class="form-group" <?php if($row['mp3_type']=='local'){?>style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                <label class="col-md-3 control-label">Song Upload :-</label>
                <div class="col-md-6">
                  <?php 
                  $mp3_file=$row['mp3_url'];

                  if($row['mp3_type']=='local'){
                    $mp3_file=$file_path.'uploads/'.basename($row['mp3_url']);
                  } 
                  ?>
                  <input type="file" name="mp3_local" id="mp3_local" value="" accept=".mp3" class="form-control">
                  <p><strong>Current URL:</strong> <?=$mp3_file?></p>
                  <div id="uploadPreview" style="background: rgba(0,0,0,0.5);text-align: center;margin-bottom: 15px;padding: 1em">


                    <audio id="audio" controls src="<?=$mp3_file?>"></audio>  
                  </div>
                </div>
              </div>
              <div id="thumbnail" class="form-group">
                <label class="col-md-3 control-label">Image:-
                  <p class="control-label-help">(Recommended resolution: 300x300,400x400 or Square Image)</p>
                </label>
                <div class="col-md-6">
                  <div class="fileupload_block">
                    <input type="file" name="mp3_thumbnail" value="" id="fileupload" onchange="fileValidation()">

                    <?php if($row['mp3_thumbnail']!='' AND file_exists('images/'.$row['mp3_thumbnail'])){?>
                      <div class="fileupload_img" id="imagePreview"><img type="image" src="images/<?php echo $row['mp3_thumbnail'];?>" alt="image" style="width: 100px;height: 100px;"/></div>
                    <?php }else{?>
                      <div class="fileupload_img" id="imagePreview"><img type="image" src="assets/images/square.jpg" alt="image" style="width: 100px;height: 100px" /></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Song Description :-</label>
                <div class="col-md-6">                    

                  <textarea name="mp3_description" id="mp3_description" class="form-control"><?php echo $row['mp3_description']?></textarea>

                  <script>
                    CKEDITOR.replace('mp3_description',{
                      filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=&akey=viaviweb',
                      filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=&akey=viaviweb',
                      filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr=&akey=viaviweb'
                    });
                  </script>

                </div>
              </div>
              <br/>
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" name="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php include("includes/footer.php");?>

<script type="text/javascript">
  $(document).ready(function(e) {
    $("#mp3_type").change(function(){
      var type=$("#mp3_type").val();
      if(type=="server_url")
      {
        $("#mp3_url_display").show();
        $("#thumbnail").show();
        $("#mp3_local_display").hide();
        $("#mp3_local").val('');
        $("#audio").attr('src','');
      }
      else
      {
        $("#mp3_url_display").hide();               
        $("#mp3_local_display").show();
        $("#thumbnail").show();
      }
    });
  });

  $("#mp3_local").change(function(e){
    var file = e.currentTarget.files[0];

    $("#filesize").text(file.size);

    objectUrl = URL.createObjectURL(file);
    $("#audio").prop("src", objectUrl);
    $("#uploadPreview").show();

  });

  function fileValidation(){
    var fileInput = document.getElementById('fileupload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/i;
    if(!allowedExtensions.exec(filePath)){
      if(filePath!=''){
        infoDlg = duDialog('Opps!', 'Please upload file having extension .png, .jpg, .jpeg .PNG, .JPG, .JPEG only!', { init: null,dark: false})
      }
      fileInput.value = '';
      return false;
    }else{
      if (fileInput.files && fileInput.files[0]) {

        var reader = new FileReader();
        reader.onload = function(e) {
          $("#imagePreview").find("img").attr("src", e.target.result);
        };
        reader.readAsDataURL(fileInput.files[0]);

      }
    }
  }
</script>       
