<?php 

$page_title="Songs Suggestion";

include("includes/header.php");
require("includes/function.php");
require("language/language.php");

$sql_query="SELECT suggest.*, user.`name` FROM tbl_song_suggest suggest 
LEFT JOIN tbl_users user
ON suggest.`user_id`=user.`id` ORDER BY suggest.`id` DESC";

$result=mysqli_query($mysqli,$sql_query) or die(mysqli_error($mysqli));
?>

<link rel="stylesheet" type="text/css" href="assets/css/stylish-tooltip.css">

<div class="row">
  <div class="col-xs-12">
    <?php
    if(isset($_SERVER['HTTP_REFERER']))
    {
      echo '<a href="'.$_SERVER['HTTP_REFERER'].'"><h4 class="pull-left" style="font-size: 20px;color: #e91e63"><i class="fa fa-arrow-left"></i> Back</h4></a>';
    }
    ?>
    <div class="card mrg_bottom">
      <div class="page_title_block">
        <div class="col-md-5 col-xs-12">
          <div class="page_title"><?=$page_title?></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12 mrg-top">
        <table class="datatable table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>User</th>
              <th>Song Title</th>
              <th>Image</th> 
              <th>Message</th>
              <th>Datetime</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
           <?php

           $i=1;
           while($row=mysqli_fetch_array($result))
           {
            ?>
            <tr>
              <td width="50"><?=$i++?></td>
              <td style="word-wrap: break-all;"><?php echo $row['name'];?></td>
              <td style="word-wrap: break-all;"><?php echo $row['song_title'];?></td>
              <td nowrap="">
                <?php 
                if(file_exists('images/'.$row['song_image'])){
                  ?>
                  <span class="mytooltip tooltip-effect-3">
                    <span class="tooltip-item">
                      <img src="images/<?php echo $row['song_image'];?>" alt="no image" style="width: 100px;height: auto;max-height: 100px;border-radius: 5px">
                    </span> 
                    <span class="tooltip-content clearfix">
                      <a href="images/<?php echo $row['song_image'];?>" target="_blank"><img src="images/<?php echo $row['song_image'];?>" alt="no image" /></a>
                    </span>
                  </span>
                <?php }else{
                  ?>
                  <img src="" alt="no image" style="width: 100px;height: auto;border-radius: 5px">
                  <?php
                } ?>
              </td>
              <td style="word-wrap: break-all;"><?php echo $row['message'];?></td> 
              <td>
                <?=(!is_null($row['created_at'])) ? date('d-m-Y', $row['created_at']).'<br/>'.date('h:i A', $row['created_at']) : '-'?>
              </td>
              <td>
                <a href="" class="btn btn-danger btn_delete btn_delete_a" data-table="tbl_song_suggest" data-id="<?php echo $row['id'];?>"  data-toggle="tooltip" data-tooltip="Delete"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
</div>            


<?php include("includes/footer.php");?>

<script type="text/javascript">
  $('a[data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'bottom',
    html: true
  });

  $(".btn_delete_a").click(function(e){

    e.preventDefault();

    var _ids=$(this).data("id");
    var _table='tbl_song_suggest';

    confirmDlg = duDialog('Are you sure?', 'This action cannot be undone, proceed?', {
      init: true,
      dark: false, 
      buttons: duDialog.OK_CANCEL,
      okText: 'Proceed',
      callbacks: {
        okClick: function(e) {
          $(".dlg-actions").find("button").attr("disabled",true);
          $(".ok-action").html('<i class="fa fa-spinner fa-pulse"></i> Please wait..');
          $.ajax({
            type:'post',
            url:'processData.php',
            dataType:'json',
            data:{id:_ids,'action':'multi_delete','tbl_nm':_table},
            success:function(res){
              location.reload();
            }
          });

        } 
      }
    });
    confirmDlg.show();
  }); 

</script>          
